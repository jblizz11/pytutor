# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

   * clone git 
   * navigate to folder 
   * run commands in terminal 
   * source venv/bin/activate to activate virtual environment 
   * pip install -r requirements.txt to install dependencies 
   * create database TimeTable 
   * run timetable.sql to populate database 
   * python run.py to start the application 


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact